def call(Map config) {
    def list_string = ""
    def jobs = Hudson.instance.instance.getView("dev-后端").items
    for(int i=0; i<jobs.size();i++){
        String result = jobs.get(i).name
        if ( i == 0 ){
            list_string = "${result}"
        } else{
            list_string = "${list_string}"+","+ "${result}"
        }
        
    }
    println "${list_string}"
  pipeline {
    agent any
    options {
        disableConcurrentBuilds abortPrevious: true
        buildDiscarder logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '1', numToKeepStr: '3')
    }
    parameters {
        choice(name:'DEPLOY_ENV', choices:'qa1\nqa2\nqa3\nqa4\nqa5\nuat\nuat2\nopen\nuat3', description:'请选择部署环境')
        extendedChoice(name: 'project_job', value: "${list_string}", defaultValue: "${list_string}",multiSelectDelimiter: ',',type: 'PT_CHECKBOX', visibleItemCount: 5)
    }
    stages {
        stage('构建') {
            steps {
                script {
                    for ( item in params.project_job.tokenize(',')){
                        echo "$item  ${DEPLOY_ENV}"
                        // build(job: "$item", propagate: false, wait: false)
                    }
                }
            }
        }
    }
  }
}
