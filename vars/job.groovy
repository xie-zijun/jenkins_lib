def call(config) {
    
    properties([
        parameters([
            [$class: 'ChoiceParameter', choiceType: 'PT_SINGLE_SELECT', 
            description: '必选项: 请选择对应的视图名称', filterLength: 1, 
            filterable: false, name: 'viewName', 
            randomName: 'choice-parameter-16477448705762715', 
            script: [
                $class: 'GroovyScript', 
                fallbackScript: 
                    [classpath: [], sandbox: false, 
                    script: 'return ["Envrionment Undefined"]'
                    ], 
                script: 
                    [classpath: [], sandbox: true, 
                    script: '''
                        def view_list=["dev-前端","dev-后端"]
                        return view_list
                    '''
                    ]
                ]
            ], 
            [$class: 'CascadeChoiceParameter', choiceType: 'PT_SINGLE_SELECT', 
            description: '必选项: 请选择对应branch分支', filterLength: 5, filterable: false, name: 'envName', 
            randomName: 'choice-parameter-9945358512870036', referencedParameters: 'viewName', 
            script: [
                $class: 'GroovyScript', 
                fallbackScript: 
                    [classpath: [], sandbox: false, 
                    script: '''return ["Error: 脚本流程失败,请检查"]'''
                    ], 
                script: [
                    classpath: [], sandbox: false, 
                    script: '''
                        def dev_list=["dev1","dev2","dev3","dev4","dev5"]
                        def qa_list=["qa1","qa2","qa3","qa4","qa5","uat","uat2"]
                        if ( viewName.equals("dev-前端")){
                            return dev_list
                        } else if ( viewName.equals("dev-后端") ){
                            return dev_list
                        }
                    '''
                    ]
                ]
            ],
            [$class: 'CascadeChoiceParameter', choiceType: 'PT_CHECKBOX', 
            description: '必选项: 选择需要构建发布的工程\n如只发布一个工程,可将工程名键入至Filter框中\n注意: 请勿键入Enter键', filterLength: 10, filterable: true, name: 'serviceName', 
            randomName: 'choice-parameter-16477448709149855', referencedParameters: 'viewName', 
            script: [
                $class: 'GroovyScript', 
                fallbackScript: 
                    [classpath: [], sandbox: false, 
                    script: '''
                        return ['Error: 脚本流程失败,请检查']
                    '''
                    ], 
                script: [
                    classpath: [], sandbox: false, 
                    script: '''
                        def removeLst = [
                            
                            ]
                        def jobs = jenkins.model.Jenkins.instance.getView("${viewName}").items
                        jobs.collect{ element -> 
                            if ( removeLst.contains(element.name) == true ){
                                    return element.name
                            } else if ( element.description.tokenize(':')[0] != '重置' ){
                                    return element.name
                            } else {
                                    return element.name + ":selected"
                              }
                        }
                        '''
                    ]
                ]
            ]
        ])
    ])
    // def jobs = getViewJobItem( viewName, serviceName)
    pipeline {
        agent any
        parameters {
            booleanParam(name: 'SYNC_GROOVY_SCRIPT',defaultValue: false, description: '同步groovy脚本, 不执行操作步骤/只打印参数检查')
        }
        stages {
            stage("输出参数信息"){
                steps{
                    script{
                        println """
##########参数信息###############
视图:     ${viewName}
环境:     ${envName}
重置服务: ${serviceName}
##########参数信息###############
                        """
                    }
                }
            }
            stage('发起构建') {
                when {
                   environment name: 'SYNC_GROOVY_SCRIPT', value: 'false'
                }
                steps {
                    script{
                           serviceName.tokenize(',').each{ job ->
                               println(job)
                           }
                        // serviceName.tokenize(',').each{ job ->
                            // println("${job.description}[0]")
                            // build(job: job, propagate: false, wait: false, parameters: [string(name: 'DEPLOY_ENV', value: envName), string(name: 'BRANCH', value: "master"),booleanParam(name: 'BUILD_AND_DEPLOY', value: "true")])
                        //}
                    }
                }
            }
        }
    }
}

// @NonCPS
// def getViewJobItem(vname, rmJob){
    // def temp_list = []
    // def jobs = jenkins.model.Jenkins.instance.getView(vname).items
    // jobs.each{ job ->temp_list.add(job.name)}
    // rmJob.tokenize(',').each{ item -> temp_list.remove(item)}
    // return  temp_list 
// }
