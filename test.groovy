def call(Map config) {
pipeline {
    agent any
    options {
        buildDiscarder(logRotator(numToKeepStr:'3', daysToKeepStr: '3'))
    }
    parameters {
        choice(name:'DEPLOY_ENV', choices:'qa1\nqa2\nqa3\nqa4\nqa5\nuat\nuat2\nopen\nuat3', description:'请选择部署环境')
        choice(name:'ViewStr', choices:'dev-前端\ndev-后端', description:'请选择部署环境')
    }
    stages {
        stage('构建') {
            steps {
                script {
                    // def ViewStr="dev-前端"
                    def jobs = Hudson.instance.instance.getView(ViewStr).items
                    jobs.each{ item ->
                        println "\nJob: $item.name $DEPLOY_ENV"
                        build(job: "$item.name", propagate: false, wait: false)
                    }
                }
            }
        }
    }
  }
}
