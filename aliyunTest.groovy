@Grab(group='com.aliyun', module='aliyun-java-sdk-core', version='4.5.25')

package com.aliyun.sample;

import com.aliyun.tea.*;
import com.aliyun.alidns20150109.*;
import com.aliyun.alidns20150109.models.*;
import com.aliyun.teaopenapi.*;
import com.aliyun.teaopenapi.models.*;


public class Sample {

    /**
     * 使用AK&SK初始化账号Client
     * @param accessKeyId
     * @param accessKeySecret
     * @return Client
     * @throws Exception
     */
    public static com.aliyun.alidns20150109.Client createClient(String accessKeyId, String accessKeySecret) throws Exception {
        Config config = new Config()
                // 您的AccessKey ID
                .setAccessKeyId(accessKeyId)
                // 您的AccessKey Secret
                .setAccessKeySecret(accessKeySecret);
        // 访问的域名
        config.endpoint = "alidns.cn-hangzhou.aliyuncs.com";
        return new com.aliyun.alidns20150109.Client(config);
    }

    public static void main(String[] args_) throws Exception {
        java.util.List<String> args = java.util.Arrays.asList(args_);
        com.aliyun.alidns20150109.Client client = Sample.createClient("LTAI5tEfmdt3ML8j9zNKVLTk", "rMAHNwJWEQAgeayuvo1T8HW1rGEnYJ");
        DescribeCustomLineRequest describeCustomLineRequest = new DescribeCustomLineRequest()
                .setLineId(1L)
                .setLang("test");
        // 复制代码运行请自行打印 API 的返回值
        client.describeCustomLine(describeCustomLineRequest);
    }
}