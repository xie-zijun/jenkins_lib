@Grapes(
  @Grab(group='com.aliyun', module='domain20180129', version='1.0.1')
)

package com.aliyun.sample

import com.aliyun.tea.*;
import com.aliyun.domain20180129.*;
import com.aliyun.domain20180129.models.*;
import com.aliyun.teaopenapi.*;
import com.aliyun.teaopenapi.models.*;

public class Sample {

    /**
     * 使用AK&SK初始化账号Client
     * @param accessKeyId
     * @param accessKeySecret
     * @return Client
     * @throws Exception
     */
    public static com.aliyun.domain20180129.Client createClient(String accessKeyId, String accessKeySecret) throws Exception {
        Config config = new Config()
                // 您的AccessKey ID
                .setAccessKeyId(accessKeyId)
                // 您的AccessKey Secret
                .setAccessKeySecret(accessKeySecret);
        // 访问的域名
        config.endpoint = "domain.aliyuncs.com";
        return new com.aliyun.domain20180129.Client(config);
    }

    public static void main(String[] args_) throws Exception {
        java.util.List<String> args = java.util.Arrays.asList(args_);
        com.aliyun.domain20180129.Client client = Sample.createClient("LTAI5tEfmdt3ML8j9zNKVLTk", "rMAHNwJWEQAgeayuvo1T8HW1rGEnYJ");
        QueryTaskDetailHistoryRequest queryTaskDetailHistoryRequest = new QueryTaskDetailHistoryRequest()
                .setLang("test")
                .setUserClientIp("test")
                .setTaskNo("test")
                .setDomainName("test")
                .setDomainNameCursor("test")
                .setTaskDetailNoCursor("test");
        // 复制代码运行请自行打印 API 的返回值
        client.queryTaskDetailHistory(queryTaskDetailHistoryRequest);
    }
}



      